<?php
$monthDays = array ('January' => 31, 'February' => 28,
'March' => 31, 'April' => 30,
'May' => 31, 'June' => 30,
'July' => 31, 'August' => 31,
'September' => 30, 'October' => 31,
'November' => 30, 'December' => 31);

//method 1
foreach ($monthDays as $keys => $value){
    echo "$keys is $value Days"."<br>";
    
    }

//method 2
echo array_search(30,$monthDays);
?>